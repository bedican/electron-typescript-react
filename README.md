# Electron boilerplate for Typescript/React

This project provides a boilerplate for an electron app using typescript and react.

```bash
# Building
$ npm run-script build

# Running
$ npm start
```
