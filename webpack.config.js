const GenerateJsonPlugin = require('generate-json-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const packagejson = require('./package.json');
const path = require('path');

module.exports = [
    {
        mode: 'development',
        entry: './src/electron.ts',
        target: 'electron-main',
        module: {
            rules: [{
                test: /^electron.ts$/,
                use: [{ loader: 'ts-loader' }]
            }]
        },
        output: {
            path: __dirname + '/dist',
            filename: 'electron.js'
        },
        resolve: {
            extensions: ['.js', '.ts', '.tsx', '.jsx', '.json']
        },
        plugins: [
            new GenerateJsonPlugin('package.json', {
              name: packagejson.name,
              version: packagejson.version,
              productName: packagejson.name,
              main: 'electron.js'
            })
        ]
    },
    {
        mode: 'development',
        entry: './src/react.tsx',
        target: 'electron-renderer',
        devtool: 'source-map',
        module: {
            rules: [{
                test: /\.tsx?$/,
                include: /src/,
                use: [{ loader: 'ts-loader' }]
            },{
                test: /\.json$/,
                include: /src/,
                use: [{ loader: 'json-loader' }]
            },{
                test: /(\.css|\.less)$/,
                include: /src/,
                use: [{
                    loader: MiniCssExtractPlugin.loader,
                    options: {
                        publicPath: path.join(__dirname, 'dist'),
                    }
                }, 'css-loader', 'less-loader']
            }]
        },
        output: {
            path: __dirname + '/dist',
            filename: 'react.js'
        },
        resolve: {
            extensions: ['.js', '.ts', '.tsx', '.jsx', '.json', '.css', '.less']
        },
        plugins: [
            new MiniCssExtractPlugin(),
            new HtmlWebpackPlugin({
                template: './src/index.html'
            })
        ]
    }
];
